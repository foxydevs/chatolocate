   

import 'dart:convert';
import 'dart:io'; 

import 'package:chatolocate/models/munis_model.dart';
import 'package:chatolocate/models/zonas_model.dart';
import 'package:http/http.dart' as http;
import 'package:mime_type/mime_type.dart';
import 'package:http_parser/http_parser.dart';
class ZonasProvider{
 
  final String _url = 'https://foxylabs-ventas.herokuapp.com/api/zona';
  final  Map<String, String> headers = {
"Content-Type": "application/x-www-form-urlencoded",  
"Content-type": "application/json"};


 Future<bool> crearZonas(ZonasModel zonas)async{
   final url = '$_url/add';
 

 
 
 final resp = await http.post(url, headers: headers, body: zonasModelToJson(zonas));
 
  final decodedData = json.decode(resp.body);
  return true;
  }


Future<List<ZonasModel>> cargarZonas() async{

  final url = '$_url/get_all';
  final resp = await http.get(url);

  final List<dynamic> decodedData = json.decode(resp.body);
  final List<ZonasModel> zonas  = new List();
  if(decodedData == null) return[];

 

  decodedData.forEach((zona) {
    final catTemp = ZonasModel.fromJson(zona);
    zonas.add(catTemp);
   
  });

  return zonas;

}


Future<bool> borrarZonas(ZonasModel zonas )async{

int id = zonas.id;

final url = '$_url/update_$id';



zonas.estado = 0;


final resp = await http.put(url, headers: headers, body: zonasModelToJson(zonas));
 
final decodedData  = json.decode(resp.body);



return true;

}



Future<bool> modificarZonas(ZonasModel zonas )async{

int id = zonas.id;

final url = '$_url/update_$id';



final resp = await http.put(url, headers: headers, body: zonasModelToJson(zonas));
 
final decodedData  = json.decode(resp.body);



return true;

} 


Future<String> subirImagen(File imagen)async {

  final url = Uri.parse('https://api.cloudinary.com/v1_1/dfe2fyrrb/image/upload?upload_preset=kcwihw9b');
  final mimeType = mime(imagen.path).split('/');


  final imageUploadRequest = http.MultipartRequest(
    'POST',
    url
  );

  final file = await http.MultipartFile.fromPath(
    'file', 
    imagen.path,
    contentType: MediaType( mimeType[0], mimeType[1] )
    );

    imageUploadRequest.files.add(file);//si copiamos muchas veces podemos adjuntar varios 

    final streamResponse = await imageUploadRequest.send();
    final resp = await http.Response.fromStream(streamResponse);

    if(resp.statusCode !=200 && resp.statusCode !=  201){
      return null;
    }

    final respData = json.decode(resp.body);
    return respData['secure_url'];

}






Future<List<ZonasModel>> cargarZonasid(String iddir) async{
 
  final url = '$_url/muni_$iddir';
   final resp = await http.get(url, headers: headers);
 

  final List<dynamic> decodedData = json.decode(resp.body);
  final List<ZonasModel> zonas  = new List();
  if(decodedData == null) return[];

 

  decodedData.forEach((zona) {
    final catTemp = ZonasModel.fromJson(zona);
    zonas.add(catTemp);
   
  });

  return zonas;

}





}
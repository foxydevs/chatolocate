    

import 'dart:convert';
import 'dart:io'; 

import 'package:chatolocate/models/colonias_model.dart';

import 'package:http/http.dart' as http;
import 'package:mime_type/mime_type.dart';
import 'package:http_parser/http_parser.dart';
class ColoniasProvider{
 
  final String _url = 'https://foxylabs-ventas.herokuapp.com/api/colonia';
  final  Map<String, String> headers = {
"Content-Type": "application/x-www-form-urlencoded",  
"Content-type": "application/json"};


 Future<int> crearColonias(ColoniasModel colonias)async{
   final url = '$_url/add';
 final resp = await http.post(url, headers: headers, body: coloniasModelToJson(colonias));
 
  final decodedData = json.decode(resp.body);
  print("valor de la colonia");
  print(decodedData);
  int idvent = decodedData['id'];
 
  return idvent;
  }


Future<List<ColoniasModel>> cargarColonias() async{

  final url = '$_url/get_all';
  final resp = await http.get(url);

  final List<dynamic> decodedData = json.decode(resp.body);
  final List<ColoniasModel> colonias  = new List();
  if(decodedData == null) return[];

 

  decodedData.forEach((colonia) {
    final catTemp = ColoniasModel.fromJson(colonia);
    colonias.add(catTemp);
   
  });

  return colonias;

}


Future<bool> borrarColonias(ColoniasModel colonias )async{

int id = colonias.id;

final url = '$_url/update_$id';



colonias.estado = 0;


final resp = await http.put(url, headers: headers, body: coloniasModelToJson(colonias));
 
final decodedData  = json.decode(resp.body);



return true;

}



Future<bool> modificarColonias(ColoniasModel colonias )async{

int id = colonias.id;

final url = '$_url/update_$id';



final resp = await http.put(url, headers: headers, body: coloniasModelToJson(colonias));
 
final decodedData  = json.decode(resp.body);



return true;

} 


Future<String> subirImagen(File imagen)async {

  final url = Uri.parse('https://api.cloudinary.com/v1_1/dfe2fyrrb/image/upload?upload_preset=kcwihw9b');
  final mimeType = mime(imagen.path).split('/');


  final imageUploadRequest = http.MultipartRequest(
    'POST',
    url
  );

  final file = await http.MultipartFile.fromPath(
    'file', 
    imagen.path,
    contentType: MediaType( mimeType[0], mimeType[1] )
    );

    imageUploadRequest.files.add(file);//si copiamos muchas veces podemos adjuntar varios 

    final streamResponse = await imageUploadRequest.send();
    final resp = await http.Response.fromStream(streamResponse);

    if(resp.statusCode !=200 && resp.statusCode !=  201){
      return null;
    }

    final respData = json.decode(resp.body);
    return respData['secure_url'];

}






Future<List<ColoniasModel>> cargarColoniasid(String iddir) async{
 
  final url = '$_url/zona_$iddir';
   final resp = await http.get(url, headers: headers);
 

  final List<dynamic> decodedData = json.decode(resp.body);
  final List<ColoniasModel> colonias  = new List();
  if(decodedData == null) return[];

 

  decodedData.forEach((colonia) {
    final catTemp = ColoniasModel.fromJson(colonia);
    colonias.add(catTemp);
   
  });

  return colonias;

}





}
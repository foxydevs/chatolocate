import 'dart:convert';


import 'package:chatolocate/preferencias_usuario/preferencias_usuario.dart';

import 'package:http/http.dart' as http;

class LoginProvider {

    final _prefs = new PreferenciasUsuario(); // guardar en storage del celular

  final  Map<String, String> headers = {
"Content-Type": "application/x-www-form-urlencoded",  
"Content-type": "application/json"};



  Future<Map<String, dynamic>> login( String email, String password) async {

    final authData = {
      'email'    : email,
      'password' : password,
      'returnSecureToken' : true
    };

    final resp = await http.post(
      'https://foxylabs-ventas.herokuapp.com/api/usuarios/tipo_login',headers: headers,
      body: json.encode( authData )
    );
 
    Map<String, dynamic> decodedResp = json.decode( resp.body );

    print(decodedResp);

    if ( decodedResp.containsKey('id') ) {
      _prefs.idvendedor = decodedResp['id_usuarios_tipos'];
      return { 'ok': true, 'token': decodedResp['id'] };
    } else {
      return { 'ok': false, 'mensaje': 'Usuario o Password erroneo' };
    }
 
  }


 



}
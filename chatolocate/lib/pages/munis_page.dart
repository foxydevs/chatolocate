
import 'package:chatolocate/bloc/munis_bloc.dart';
import 'package:chatolocate/bloc/provider.dart';
import 'package:chatolocate/models/depto_model.dart';
import 'package:chatolocate/models/munis_model.dart';
import 'package:chatolocate/pages/depto_page.dart';
import 'package:chatolocate/pages/munisin_page.dart';
import 'package:chatolocate/pages/zonas_page.dart';
import 'package:chatolocate/preferencias_usuario/preferencias_usuario.dart';
import 'package:flutter/material.dart';
 







class MunisPage extends StatefulWidget {
//final categoriasModel = CategoriasProvider();
 //final categoriasProvider = new CategoriasProvider();

  @override
  _MunisPageState createState() => _MunisPageState();
}
 MunisModel munis = new MunisModel();
  int idusuario = 0;
  dynamic muni = "";
class _MunisPageState extends State<MunisPage> {
void initState() {
    super.initState();
   
  }
  @override 
  
  Widget build(BuildContext context) { 

   final munisBloc = Provider.munisBloc(context);
    
  final DeptoModel prodData = ModalRoute.of(context).settings.arguments;  //verificar si vienen argumentos 
    if(prodData!=null){
      
      idusuario = prodData.id;
   
    
    }
 
  cargarmunisid();
  


       return Scaffold(
         appBar: AppBar(
       
        backgroundColor: Colors.black,
        title: Text('Municipios'),
        
        ),
      body:  _crearListado(munisBloc),
      floatingActionButton: _crearBoton(context, idusuario),
      
    );
  }

  Widget _crearListado( MunisBloc munisBloc){ 

   
      
  
    return  
    RefreshIndicator(
            onRefresh:  (){

                return  munisBloc.cargarMunisid(idusuario.toString());
                }, 
                 
          child: 
          
          StreamBuilder(

        stream: munisBloc.munisStream,
        
         
        builder: (BuildContext context, AsyncSnapshot<List<MunisModel>> snapshot){
            muni  = snapshot.data;
          if(snapshot.hasData){
            return  
            
            ListView.builder(
            itemCount: muni.length,
            itemBuilder: (context, i) => _crearItem(context, muni[i], munisBloc),
            );
          } 
          else{
            return Center(child: CircularProgressIndicator(),);
          }
          
        },
      ),
    );

  
 
  }
 
  cargarmunisid() async {
    final munisBloc = Provider.munisBloc(context);
  await munisBloc.cargarMunisid(idusuario.toString());
  }
Widget _crearItem(BuildContext context, MunisModel munis, MunisBloc munisBloc){

  return Dismissible(
      key: UniqueKey(),
      background: Container(
        color: Colors.red,
      ),
      onDismissed: (mun){
        //categoriasProvider.borrarCategoria(categorias);
          Navigator.push(context, MaterialPageRoute(builder: (context) => MunisinPage(), settings: RouteSettings(arguments: munis))).then((value) {
  setState(() {
  
       // refresh state 
         
    });
  }); 
          
      },
      child: Card( 
        child:  Column(
          children: <Widget>[  
            
               new ListTile(
                 leading: Icon(Icons.location_city),
      title:  Text('Municipi: ${munis.nombre}'),
   
      onTap: () async {
 
        await cargarzonasid(munis.id);
      Navigator.push(context, MaterialPageRoute(builder: (context) => ZonasPage(), settings: RouteSettings(arguments: munis))).then((value) {
  setState(() {
  
       // refresh state 
         
    });
  }); 
       }
    )
             
  
           
         
          ]
        ),
      ) 


  );

} 

  _crearBoton(BuildContext context, int idusuario){

         final _prefs = new PreferenciasUsuario();
     if (_prefs.idvendedor == 2) {
       
    
    return FloatingActionButton(
      
      backgroundColor: Colors.black, 
       
      child: Icon(Icons.add, color: Colors.lightGreenAccent,),
      onPressed: () {
        munis.idDepartamento = idusuario;
        munis.nombre = "";
        Navigator.push(context, MaterialPageRoute(builder: (context) => MunisinPage(), settings: RouteSettings(arguments: munis))).then((value) {
  setState(() {
      // refresh state
    });
  });
  }
  
      );
     }
    }

    cargarzonasid(int idzonas) async {
    final zonasBloc = Provider.zonasBloc(context);
  await zonasBloc.cargarZonasid(idzonas.toString());
  }
}
  

import 'dart:io';

import 'package:chatolocate/bloc/depto_bloc.dart';
import 'package:chatolocate/bloc/munis_bloc.dart';
import 'package:chatolocate/bloc/provider.dart';
import 'package:chatolocate/models/depto_model.dart';
import 'package:chatolocate/models/munis_model.dart';
import 'package:chatolocate/pages/depto_page.dart';
import 'package:flutter/material.dart';




class MunisinPage extends StatefulWidget {
  
  @override
  _MunisinPageState createState() => _MunisinPageState();
}

class _MunisinPageState extends State<MunisinPage> {
  final formKey = GlobalKey<FormState>();
  final scaffoldKey= GlobalKey<ScaffoldState>();
   String dropdownValue = '1,Cliente'; // primer valor
 
  MunisBloc munisBloc;  //bloc implementar 
  MunisModel munis = new MunisModel();
  bool _guardando = false; 
  File foto;
  int idcategori=0;
  int boton = 0; 
  @override
  Widget build(BuildContext context) {

    munisBloc = Provider.munisBloc(context);
   final MunisModel prodData = ModalRoute.of(context).settings.arguments;  //verificar si vienen argumentos 
   
 
    if(prodData!=null){
      munis = prodData;
       boton = 1; 
    }

    
    return Scaffold(
      key: scaffoldKey,
   
      appBar: AppBar(
       
        backgroundColor: Colors.black,
        title: Text('Direccion'),
        
        actions: 
        
        <Widget>[
            

        
            
            
        ],
      ),
      body: SingleChildScrollView(
       child: Container(
         padding: EdgeInsets.all(15.0),
         child: Form(
           key: formKey, // identificador unico de este form 
           child: Column(
              children: <Widget>[
                 //_mostrarFoto(),
                  _crearNombre(),
                 // _crearApellido(),
                  //_crearEmail(),
                  //_crearPrecio(),
                  //_crearTipousu(prodData),
                  _crearActivo(), 
                  _crearBoton()

                    
                 
                  
                  
                
                 
          
              ]
             
           ))
       )
      ),
     
      
    );
  }

Widget _crearNombre()
{
return TextFormField(
  initialValue: munis.nombre,
  textCapitalization: TextCapitalization.sentences,
  decoration: InputDecoration( 
    labelText: 'Municipio'
  ),
  onSaved: (value) => munis.nombre = value,
  validator: (value){
    if(value.length < 3 ){
      return 'Ingrese Municipios';
    }else{
      return null;
    }
  },
);
}






Widget _crearActivo(){
   bool catest = true;
  

  return SwitchListTile(

    
    value: catest, 
    title: Text('Estado'),
    activeColor: Colors.green,
    onChanged: (value) => setState((){
     if(value == true){
          munis.estado = 1; 
        }
        else{
          munis.estado = 0; 
        }
    })
    );


}

Widget  _crearBoton(){
  return RaisedButton.icon(
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
    color: Colors.black,
    textColor: Colors.white,
    onPressed:(_guardando)? null:_submit, icon: Icon(Icons.save), label: Text('Guardar')

    );
}

  

void _submit ()async{ // para disparar los mensajes 


if(!formKey.currentState.validate())return; //si es invalido manda error regresa true si es valido el formulario o false si no es valido

  formKey.currentState.save();
    // para bloquear boton de guardar 

  setState(() {
   _guardando = true; //para redibujar widget  
  });
  

 
 
  if(munis.id == null){
   munisBloc.agregarMunis(munis);
    print("ID ID");
  }
  else{
    munisBloc.editarMunis(munis);
    print("NO ID");
  }

   
  mostrarSnackbar('Guardado');

     Navigator.pop(context);
   

 
  

}



void mostrarSnackbar(String mensaje){
/// mostrar mensaje al guardar 
  final snackbar = SnackBar(

    content: Text(mensaje),
    duration: Duration(milliseconds: 1500),
  );

  scaffoldKey.currentState.showSnackBar(snackbar);

}



}

import 'package:chatolocate/bloc/munis_bloc.dart';
import 'package:chatolocate/bloc/provider.dart';
import 'package:chatolocate/bloc/zonas_bloc.dart';
import 'package:chatolocate/models/depto_model.dart';
import 'package:chatolocate/models/munis_model.dart';
import 'package:chatolocate/models/zonas_model.dart';
import 'package:chatolocate/pages/colonias_page.dart';
import 'package:chatolocate/pages/depto_page.dart';
import 'package:chatolocate/pages/munisin_page.dart';
import 'package:chatolocate/pages/zonasin_page.dart';
import 'package:chatolocate/preferencias_usuario/preferencias_usuario.dart';
import 'package:flutter/material.dart';
 







class ZonasPage extends StatefulWidget {
//final categoriasModel = CategoriasProvider();
 //final categoriasProvider = new CategoriasProvider();

  @override
  _ZonasPageState createState() => _ZonasPageState();
} 
 ZonasModel zonas = new ZonasModel();
  int idusuario = 0;
  dynamic zona = "";
class _ZonasPageState extends State<ZonasPage> {
void initState() {
    super.initState();
   
  }
  @override 
  
  Widget build(BuildContext context) { 

   final zonasBloc = Provider.zonasBloc(context);
    
  final MunisModel prodData = ModalRoute.of(context).settings.arguments;  //verificar si vienen argumentos 
    if(prodData!=null){
      
      idusuario = prodData.id;
   
    
    }
 
  cargarzonasid();
  


       return Scaffold(
         appBar: AppBar(
       
        backgroundColor: Colors.black,
        title: Text('ZONAS'),
        
        ),
      body:  _crearListado(zonasBloc),
      floatingActionButton: _crearBoton(context, idusuario),
      
    );
  }

  Widget _crearListado( ZonasBloc zonasBloc){ 

   
      
  
    return  
    RefreshIndicator(
            onRefresh:  (){

                return  zonasBloc.cargarZonasid(idusuario.toString());
                }, 
                 
          child: 
          
          StreamBuilder(

        stream: zonasBloc.zonasStream,
        
         
        builder: (BuildContext context, AsyncSnapshot<List<ZonasModel>> snapshot){
            zona  = snapshot.data;
          if(snapshot.hasData){
            return  
            
            ListView.builder(
            itemCount: zona.length,
            itemBuilder: (context, i) => _crearItem(context, zona[i], zonasBloc),
            );
          } 
          else{
            return Center(child: CircularProgressIndicator(),);
          }
          
        },
      ),
    );

  
 
  }
 
  cargarzonasid() async {
    final zonasBloc = Provider.zonasBloc(context);
  await zonasBloc.cargarZonasid(idusuario.toString());
  }
Widget _crearItem(BuildContext context, ZonasModel zonas, ZonasBloc zonasBloc){

  return Dismissible(
      key: UniqueKey(),
      background: Container(
        color: Colors.red,
      ),
      onDismissed: (zon){
        //categoriasProvider.borrarCategoria(categorias);
        zonasBloc.borrarZonas(zonas);
          
      },
      child: Card( 
        child:  Column(
          children: <Widget>[  
            
               new ListTile(
                 leading: Icon(Icons.location_city),
      title:  Text('Zonas: ${zonas.nombre}'),
   
      onTap: () async {
 
          await cargarcoloniasid(zonas.id);
      Navigator.push(context, MaterialPageRoute(builder: (context) => ColoniasPage(), settings: RouteSettings(arguments: zonas))).then((value) {
  setState(() {
   
       // refresh state 
         
    });
  }); 
       }
    )
             
  
           
         
          ]
        ),
      ) 


  );

} 

  _crearBoton(BuildContext context, int idusuario){
         final _prefs = new PreferenciasUsuario();
     if (_prefs.idvendedor == 2) {
       
    
    return FloatingActionButton(
      
      backgroundColor: Colors.black, 
      
      child: Icon(Icons.add, color: Colors.lightGreenAccent,),
      onPressed: () {
        zonas.idMunicipio = idusuario;
        zonas.nombre = "";
          Navigator.push(context, MaterialPageRoute(builder: (context) => ZonasinPage(), settings: RouteSettings(arguments: zonas))).then((value) {
  setState(() {
      // refresh state
    });
  });
      
  }
  
      );
     }
    }
  cargarcoloniasid(int idcolonias) async {
    final coloniasBloc = Provider.coloniasBloc(context);
  await coloniasBloc.cargarColoniasid(idcolonias.toString());
  }
   
}
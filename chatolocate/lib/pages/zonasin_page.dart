   

import 'dart:io';

import 'package:chatolocate/bloc/depto_bloc.dart';
import 'package:chatolocate/bloc/munis_bloc.dart';
import 'package:chatolocate/bloc/provider.dart';
import 'package:chatolocate/bloc/zonas_bloc.dart';
import 'package:chatolocate/models/depto_model.dart';
import 'package:chatolocate/models/munis_model.dart';
import 'package:chatolocate/models/zonas_model.dart';
import 'package:chatolocate/pages/depto_page.dart';
import 'package:flutter/material.dart';




class ZonasinPage extends StatefulWidget {
  
  @override
  _ZonasinPageState createState() => _ZonasinPageState();
}

class _ZonasinPageState extends State<ZonasinPage> {
  final formKey = GlobalKey<FormState>();
  final scaffoldKey= GlobalKey<ScaffoldState>();
   String dropdownValue = '1,Cliente'; // primer valor
 
  ZonasBloc zonasBloc;  //bloc implementar 
  ZonasModel zonas = new ZonasModel();
  bool _guardando = false; 
  File foto;
  int idcategori=0;
  int boton = 0; 
  @override
  Widget build(BuildContext context) {

    zonasBloc = Provider.zonasBloc(context);
   final ZonasModel prodData = ModalRoute.of(context).settings.arguments;  //verificar si vienen argumentos 
   
 
    if(prodData!=null){
      zonas = prodData;
       boton = 1;
    }

    
    return Scaffold(
      key: scaffoldKey,
   
      appBar: AppBar(
       
        backgroundColor: Colors.black,
        title: Text('ZONAS'),
        
        actions: 
        
        <Widget>[
            

        
            
            
        ],
      ),
      body: SingleChildScrollView(
       child: Container(
         padding: EdgeInsets.all(15.0),
         child: Form(
           key: formKey, // identificador unico de este form 
           child: Column(
              children: <Widget>[
                 //_mostrarFoto(),
                  _crearNombre(),
                 // _crearApellido(),
                  //_crearEmail(),
                  //_crearPrecio(),
                  //_crearTipousu(prodData),
                  _crearActivo(), 
                  _crearBoton()

                    
                 
                  
                  
                
                 
          
              ]
             
           ))
       )
      ),
     
      
    );
  }

Widget _crearNombre()
{
return TextFormField(
  initialValue: zonas.nombre,
  textCapitalization: TextCapitalization.sentences,
  decoration: InputDecoration( 
    labelText: 'zonas'
  ),
  onSaved: (value) => zonas.nombre = value,
  validator: (value){
    if(value.length < 3 ){
      return 'Ingrese Zonas';
    }else{
      return null;
    }
  },
);
}






Widget _crearActivo(){
   bool catest = true;
  

  return SwitchListTile(

    
    value: catest, 
    title: Text('Estado'),
    activeColor: Colors.green,
    onChanged: (value) => setState((){
     if(value == true){
          zonas.estado = 1; 
        }
        else{
          zonas.estado = 0; 
        }
    })
    );


}

Widget  _crearBoton(){
  return RaisedButton.icon(
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
    color: Colors.black,
    textColor: Colors.white,
    onPressed:(_guardando)? null:_submit, icon: Icon(Icons.save), label: Text('Guardar')

    );
}

  

void _submit ()async{ // para disparar los mensajes 


if(!formKey.currentState.validate())return; //si es invalido manda error regresa true si es valido el formulario o false si no es valido

  formKey.currentState.save();
    // para bloquear boton de guardar 

  setState(() {
   _guardando = true; //para redibujar widget  
  });
  

 
 
  if(zonas.id == null){
   zonasBloc.agregarZonas(zonas);
    print("ID ID");
  }
  else{
    zonasBloc.editarZonas(zonas);
    print("NO ID");
  }

    
  mostrarSnackbar('Guardado');

     Navigator.pop(context);
   

 
  

}



void mostrarSnackbar(String mensaje){
/// mostrar mensaje al guardar 
  final snackbar = SnackBar(

    content: Text(mensaje),
    duration: Duration(milliseconds: 1500),
  );

  scaffoldKey.currentState.showSnackBar(snackbar);

}



}
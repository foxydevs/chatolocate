    

import 'dart:io';


import 'package:chatolocate/bloc/colonias_bloc.dart';
import 'package:chatolocate/bloc/provider.dart';
import 'package:chatolocate/bloc/zonas_bloc.dart';
import 'package:chatolocate/models/colonias_model.dart';

import 'package:chatolocate/models/zonas_model.dart';
import 'package:chatolocate/pages/sucursales_page.dart';
import 'package:chatolocate/provider/colonias_provider.dart';

import 'package:flutter/material.dart';




class ColoniasinPage extends StatefulWidget {
  
  @override
  _ColoniasinPageState createState() => _ColoniasinPageState();
}

class _ColoniasinPageState extends State<ColoniasinPage> {
  final formKey = GlobalKey<FormState>();
  final scaffoldKey= GlobalKey<ScaffoldState>();
   String dropdownValue = '1,Cliente'; // primer valor
 
  ColoniasBloc coloniasBloc;  //bloc implementar 
  ColoniasModel colonias = new ColoniasModel();
  bool _guardando = false;  
  File foto;
  int idcategori=0;
  int boton = 0; 
  @override
  Widget build(BuildContext context) {

    coloniasBloc = Provider.coloniasBloc(context);
   final ColoniasModel prodData = ModalRoute.of(context).settings.arguments;  //verificar si vienen argumentos 
   
 
    if(prodData!=null){
      colonias = prodData;
       boton = 1;
    }

    
    return Scaffold(
      key: scaffoldKey,
   
      appBar: AppBar(
       
        backgroundColor: Colors.black,
        title: Text('COLONIAS'),
        
        actions: 
        
        <Widget>[
            

        
            
            
        ],
      ),
      body: SingleChildScrollView(
       child: Container(
         padding: EdgeInsets.all(15.0),
         child: Form(
           key: formKey, // identificador unico de este form 
           child: Column(
              children: <Widget>[
                 //_mostrarFoto(),
                  _crearNombre(),
                 // _crearApellido(),
                  //_crearEmail(),
                  //_crearPrecio(),
                  //_crearTipousu(prodData),
                  _crearActivo(), 
                  _crearBoton()

                    
                 
                  
                  
                
                 
          
              ]
             
           ))
       )
      ),
     
      
    );
  }

Widget _crearNombre()
{
return TextFormField(
  initialValue: colonias.nombre,
  textCapitalization: TextCapitalization.sentences,
  decoration: InputDecoration( 
    labelText: 'colonias'
  ),
  onSaved: (value) => colonias.nombre = value,
  validator: (value){
    if(value.length < 3 ){
      return 'Ingrese Colonia';
    }else{
      return null;
    }
  },
);
}






Widget _crearActivo(){
   bool catest = true;
  

  return SwitchListTile(

    
    value: catest, 
    title: Text('Estado'),
    activeColor: Colors.green,
    onChanged: (value) => setState((){
     if(value == true){
          colonias.estado = 1; 
        }
        else{
          colonias.estado = 0; 
        }
    })
    );


}

Widget  _crearBoton(){
  return RaisedButton.icon(
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
    color: Colors.black,
    textColor: Colors.white,
    onPressed:(_guardando)? null:_submit, icon: Icon(Icons.save), label: Text('Guardar')

    );
}

  

void _submit ()async{ // para disparar los mensajes 
  int  idcolonia = 0;

if(!formKey.currentState.validate())return; //si es invalido manda error regresa true si es valido el formulario o false si no es valido

  formKey.currentState.save();
    // para bloquear boton de guardar 

  setState(() {
   _guardando = true; //para redibujar widget  
  });
  

 
 
  if(colonias.id == null){

    
    idcolonia = await ColoniasProvider().crearColonias(colonias);
    print("ID ID");
  }
  else{
    coloniasBloc.editarColonias(colonias);
    print("NO ID");
  }

      
  mostrarSnackbar('Guardado');
 
    // Navigator.pop(context);
   
       Navigator.push(context, MaterialPageRoute(builder: (context) => SucursalesPage(),settings: RouteSettings(arguments: idcolonia))).then((value) {
  setState(() {
      // refresh state
      Navigator.pop(context);
    });
  });
 
  

}



void mostrarSnackbar(String mensaje){
/// mostrar mensaje al guardar 
  final snackbar = SnackBar(

    content: Text(mensaje),
    duration: Duration(milliseconds: 1500),
  );

  scaffoldKey.currentState.showSnackBar(snackbar);

}



}
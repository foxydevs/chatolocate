

import 'package:chatolocate/bloc/colonias_bloc.dart';
import 'package:chatolocate/bloc/provider.dart';
import 'package:chatolocate/bloc/zonas_bloc.dart';
import 'package:chatolocate/models/colonias_model.dart';
import 'package:chatolocate/models/munis_model.dart';
import 'package:chatolocate/models/zonas_model.dart';
import 'package:chatolocate/pages/coloniasin_page.dart';
import 'package:chatolocate/pages/zonasin_page.dart';
import 'package:chatolocate/preferencias_usuario/preferencias_usuario.dart';
import 'package:flutter/material.dart';
 







class ColoniasPage extends StatefulWidget {
//final categoriasModel = CategoriasProvider();
 //final categoriasProvider = new CategoriasProvider();

  @override
  _ColoniasPageState createState() => _ColoniasPageState();
} 
 ColoniasModel colonias = new ColoniasModel();
  int idusuario = 0;
  dynamic colonia = "";
class _ColoniasPageState extends State<ColoniasPage> {
void initState() {
    super.initState();
   
  }
  @override 
  
  Widget build(BuildContext context) { 

   final coloniasBloc = Provider.coloniasBloc(context);
    
  final ZonasModel prodData = ModalRoute.of(context).settings.arguments;  //verificar si vienen argumentos 
    if(prodData!=null){
      
      idusuario = prodData.id;
   
    
    }
 
  cargarColoniasid();
  


       return Scaffold(
         appBar: AppBar(
       
        backgroundColor: Colors.black,
        title: Text('COLONIAS'),
        
        ),
      body:  _crearListado(coloniasBloc),
      floatingActionButton: _crearBoton(context, idusuario),
      
    );
  }

  Widget _crearListado( ColoniasBloc coloniasBloc){ 

   
      
  
    return  
    RefreshIndicator(
            onRefresh:  (){

                return  coloniasBloc.cargarColoniasid(idusuario.toString());
                }, 
                 
          child: 
          
          StreamBuilder(

        stream: coloniasBloc.coloniasStream,
        
         
        builder: (BuildContext context, AsyncSnapshot<List<ColoniasModel>> snapshot){
            colonia  = snapshot.data;
          if(snapshot.hasData){
            return  
            
            ListView.builder(
            itemCount: colonia.length,
            itemBuilder: (context, i) => _crearItem(context, colonia[i], coloniasBloc),
            );
          } 
          else{
            return Center(child: CircularProgressIndicator(),);
          }
          
        },
      ),
    );

  
 
  }
 
 
Widget _crearItem(BuildContext context, ColoniasModel colonias, ColoniasBloc coloniasBloc){

  return Dismissible(
      key: UniqueKey(),
      background: Container(
        color: Colors.red,
      ),
      onDismissed: (zon){
        //categoriasProvider.borrarCategoria(categorias);
        coloniasBloc.borrarColonias(colonias);
          
      },
      child: Card( 
        child:  Column(
          children: <Widget>[  
            
               new ListTile(
                 leading: Icon(Icons.location_city),
      title:  Text('Colonia: ${colonias.nombre}'),
   
      onTap: () {
       }
    )
             
  
           
         
          ]
        ),
      ) 


  );

} 

  _crearBoton(BuildContext context, int idusuario){
         final _prefs = new PreferenciasUsuario();
     if (_prefs.idvendedor == 2) {
       
    
    return FloatingActionButton(
      
      backgroundColor: Colors.black, 
      
      child: Icon(Icons.add, color: Colors.lightGreenAccent,),
      onPressed: () {
        colonias.idZona = idusuario;
        colonias.nombre = "";
          Navigator.push(context, MaterialPageRoute(builder: (context) => ColoniasinPage(), settings: RouteSettings(arguments: colonias))).then((value) {
  setState(() {
      // refresh state
    });
  });
  
  } 
  
      );
     }
    }

    cargarColoniasid() async {
    final coloniasBloc = Provider.coloniasBloc(context);
  await coloniasBloc.cargarColoniasid(idusuario.toString());
  }
}
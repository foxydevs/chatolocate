 import 'dart:ffi';


import 'package:chatolocate/bloc/provider.dart';
import 'package:chatolocate/bloc/sucursales_bloc.dart';
import 'package:chatolocate/models/coloniassucural_model.dart';
import 'package:chatolocate/models/sucursales_model.dart';
import 'package:chatolocate/provider/sucursales_provider.dart';
import 'package:flutter/material.dart';
 





class SucursalesPage extends StatefulWidget {
//final categoriasModel = CategoriasProvider();
 //final categoriasProvider = new CategoriasProvider();

  @override
  _SucursalesPageState createState() => _SucursalesPageState();
}


  int idusuario = 0;
  String idusuariovent = "" ;
  String iddireccionvent = "" ;
  double totalvent = 0.00;
  ColoniasSucursalesModel coloniassucursales = new ColoniasSucursalesModel();

  String nombr = ""; 
  String nit = "";
  String tel = "";
  int idcolonias = 0 ;
class _SucursalesPageState extends State<SucursalesPage> {
void initState() {
    super.initState();
   
  }  
  @override 
   
  Widget build(BuildContext context) { 

    final sucursalesBloc = Provider.sucursalesBloc(context);

    
  final int prodData = ModalRoute.of(context).settings.arguments;  //verificar si vienen argumentos 
   
    if(prodData!=null){
    idcolonias = prodData;
    }
      /*  final mandadosBloc = Provider.mandadosBloc(context);
    if(prodData!=null){
      
      mandados.taxisMandadosDescripcion = prodData.envio;
      mandados.taxis_usuarios_pedir_direccion = prodData.direcc;
      mandados.taxisMandadosDescripcion = prodData.envio;
      idusuariovent = prodData.idusuario;
      iddireccionvent = prodData.iddirecc;
      totalvent = prodData.total;
    } */


sucursalesBloc.cargarSucursales();



       return Scaffold(
         appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Colors.black,
        title: Text('SUCURSALES'),
        
        ),
      body:  _crearListado(sucursalesBloc), 
      //floatingActionButton: _crearBoton(context, idusuario),
      
    );
  }

  Widget _crearListado( SucursalesBloc sucursalesBloc){ 


    return  
    RefreshIndicator(
            onRefresh: sucursalesBloc.cargarSucursales,
          child: StreamBuilder( 
        stream: sucursalesBloc.sucursalStream,
        
        builder: (BuildContext context, AsyncSnapshot<List<Sucursales>> snapshot){
          final sucursal  = snapshot.data;
          if(snapshot.hasData){
            return  
            
            ListView.builder(
            itemCount: sucursal.length,
            itemBuilder: (context, i) => _crearItem(context, sucursal[i], sucursalesBloc),
            );
          } 
          else{
            return Center(child: CircularProgressIndicator(),);
          }
          
        },
      ),
    );

   

  }

Widget _crearItem(BuildContext context, Sucursales sucursales, SucursalesBloc sucursalesBloc){

  return Dismissible(
      key: UniqueKey(),
      background: Container(
        color: Colors.red,
      ),
     
      child: Card( 
        child:  Column(
          children: <Widget>[  
             
               new ListTile(
                 leading: Icon(Icons.location_city),
      title:  Text('Direccion: ${sucursales.taxisUsuariosDirecciones}'),
   
      onTap: ()  async { 
             

                coloniassucursales.idColonia = idcolonias;
                coloniassucursales.idSucursal = sucursales.taxis_usuarios_direcciones_id;
                coloniassucursales.estado = 1;
         int   idcolonia = await SucursalesProvider().crearColoniasSucursal(coloniassucursales); 
      
       
   
     


       
     Navigator.pop(context);
   
   
       }
    )
             
  
           
         
          ]
        ),
      ) 


  );

}

  _crearBoton(BuildContext context, int idusuario){
    return FloatingActionButton(
      
      backgroundColor: Colors.black, 
      
      child: Icon(Icons.add, color: Colors.lightGreenAccent,),
      onPressed: () {
        
        Navigator.push(context, MaterialPageRoute(builder: (context) => SucursalesPage())).then((value) {
  setState(() {
      // refresh state
    });
  });}
  
      );
    }

   
}
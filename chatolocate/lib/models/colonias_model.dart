// To parse this JSON data, do
//
//     final coloniasModel = coloniasModelFromJson(jsonString);

import 'dart:convert';

ColoniasModel coloniasModelFromJson(String str) => ColoniasModel.fromJson(json.decode(str));

String coloniasModelToJson(ColoniasModel data) => json.encode(data.toJson());

class ColoniasModel {
    int id;
    String nombre;
    String descripcion;
    int idZona;
    int estado;

    ColoniasModel({
        this.id,
        this.nombre = "",
        this.descripcion =  "",
        this.idZona  = 0,
        this.estado  = 0,
    });

    factory ColoniasModel.fromJson(Map<String, dynamic> json) => ColoniasModel(
        id: json["id"],
        nombre: json["nombre"],
        descripcion: json["descripcion"],
        idZona: json["id_zona"],
        estado: json["estado"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "nombre": nombre,
        "descripcion": descripcion,
        "id_zona": idZona,
        "estado": estado,
    };
}

// To parse this JSON data, do
//
//     final munisModel = munisModelFromJson(jsonString);

import 'dart:convert';

MunisModel munisModelFromJson(String str) => MunisModel.fromJson(json.decode(str));

String munisModelToJson(MunisModel data) => json.encode(data.toJson());

class MunisModel {
    int id;
    String nombre;
    String descripcion;
    int idDepartamento;
    int estado;

    MunisModel({
        this.id ,
        this.nombre = " ",
        this.descripcion = " ",
        this.idDepartamento = 0,
        this.estado = 1,
    });

    factory MunisModel.fromJson(Map<String, dynamic> json) => MunisModel(
        id: json["id"],
        nombre: json["nombre"],
        descripcion: json["descripcion"],
        idDepartamento: json["id_departamento"],
        estado: json["estado"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "nombre": nombre,
        "descripcion": descripcion,
        "id_departamento": idDepartamento,
        "estado": estado,
    };
}

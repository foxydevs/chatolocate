// To parse this JSON data, do
//
//     final coloniasSucursalesModel = coloniasSucursalesModelFromJson(jsonString);

import 'dart:convert';

ColoniasSucursalesModel coloniasSucursalesModelFromJson(String str) => ColoniasSucursalesModel.fromJson(json.decode(str));

String coloniasSucursalesModelToJson(ColoniasSucursalesModel data) => json.encode(data.toJson());

class ColoniasSucursalesModel {
    int id;
    int idColonia;
    int idSucursal;
    int estado;

    ColoniasSucursalesModel({
        this.id,
        this.idColonia,
        this.idSucursal,
        this.estado,
    });

    factory ColoniasSucursalesModel.fromJson(Map<String, dynamic> json) => ColoniasSucursalesModel(
        id: json["id"],
        idColonia: json["id_colonia"],
        idSucursal: json["id_sucursal"],
        estado: json["estado"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "id_colonia": idColonia,
        "id_sucursal": idSucursal,
        "estado": estado,
    };
}

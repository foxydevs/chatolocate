// To parse this JSON data, do
//
//     final zonasModel = zonasModelFromJson(jsonString);

import 'dart:convert';

ZonasModel zonasModelFromJson(String str) => ZonasModel.fromJson(json.decode(str));

String zonasModelToJson(ZonasModel data) => json.encode(data.toJson());

class ZonasModel {
    int id;
    String nombre;
    String descripcion;
    int idMunicipio;
    int estado;

    ZonasModel({
        this.id,
        this.nombre = "",
        this.descripcion = "",
        this.idMunicipio= 0,
        this.estado = 0,
    });

    factory ZonasModel.fromJson(Map<String, dynamic> json) => ZonasModel(
        id: json["id"],
        nombre: json["nombre"],
        descripcion: json["descripcion"],
        idMunicipio: json["id_municipio"],
        estado: json["estado"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "nombre": nombre,
        "descripcion": descripcion,
        "id_municipio": idMunicipio,
        "estado": estado,
    };
}

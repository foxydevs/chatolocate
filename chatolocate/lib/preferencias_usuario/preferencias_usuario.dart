import 'package:shared_preferences/shared_preferences.dart';

/*
  Recordar instalar el paquete de:
    shared_preferences:

  Inicializar en el main
    final prefs = new PreferenciasUsuario();
    await prefs.initPrefs();
    
    Recuerden que el main() debe de ser async {...

*/

class PreferenciasUsuario {

  static final PreferenciasUsuario _instancia = new PreferenciasUsuario._internal();

  factory PreferenciasUsuario() {
    return _instancia;
  }
 
  PreferenciasUsuario._internal();

  SharedPreferences _prefs;

  initPrefs() async {
    this._prefs = await SharedPreferences.getInstance();
  }

  // GET y SET del nombre
  get idvendedor {
    return _prefs.getInt('idvendedor') ?? '';
  } 
 
  set idvendedor( int value ) {
    _prefs.setInt('idvendedor', value);
  }

   get returnpag {
    return _prefs.getInt('returnpag') ?? '';
  }

  set returnpag( int value ) {
    _prefs.setInt('returnpag', value);
  }
  

 

}


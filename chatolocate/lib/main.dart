
import 'package:chatolocate/bloc/provider.dart';
import 'package:chatolocate/pages/depto_page.dart';
import 'package:chatolocate/pages/deptoin_page.dart';

import 'package:chatolocate/pages/logins_page.dart';
import 'package:chatolocate/pages/munis_page.dart';
import 'package:chatolocate/preferencias_usuario/preferencias_usuario.dart';
import 'package:flutter/material.dart';



 
 
void main() async {


  WidgetsFlutterBinding.ensureInitialized(); // para grabar en storage
  final prefs = new PreferenciasUsuario();
  await prefs.initPrefs();
  runApp(MyApp());

}
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
     
    return Provider(
          child: MaterialApp(
        debugShowCheckedModeBanner:  false,
        title: 'Material App',
        initialRoute: 'login',
        routes: {
      
          
           'login'   : (BuildContext context) => LoginPage(),
           'depto'   : (BuildContext context) => DeptoPage(),
           'deptoin' : (BuildContext context) => DeptoinPage(),
           'munis'   : (BuildContext context) => MunisPage(),
          
            
        },
      ),
    );
  }
}
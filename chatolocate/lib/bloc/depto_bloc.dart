   
import 'dart:io';





import 'package:chatolocate/models/depto_model.dart';
import 'package:chatolocate/provider/depto_provider.dart';
import 'package:rxdart/subjects.dart';
 
class DeptoBloc{

final _deptoController = new BehaviorSubject<List<DeptoModel>>();
final _cargandoController = new BehaviorSubject<bool>();

final _deptoProvider = new DeptoProvider();

Stream<List<DeptoModel>> get deptoStream => _deptoController.stream;

Stream<bool> get cargando => _cargandoController.stream;

Future<Null> cargarDepto()async{

final deptos = await _deptoProvider.cargarDepto();
_deptoController.sink.add(deptos);

}
Future<Null> cargarDeptosid(String iddir)async{

final deptos = await _deptoProvider.cargarUsuariosDireccionesid(iddir);
_deptoController.sink.add(deptos);

}

 agregarDeptos(DeptoModel deptos)async{
 
 _cargandoController.sink.add(true);
 await _deptoProvider.crearDepto(deptos);
 _cargandoController.sink.add(false);

}
//void cuando no regresa nada 
Future subirFoto(File foto)async{

 _cargandoController.sink.add(true);
 final fotoUrl = await _deptoProvider.subirImagen(foto); 
 _cargandoController.sink.add(false);
  return fotoUrl;
}

void editarDeptos(DeptoModel deptos)async{
 
 _cargandoController.sink.add(true);
 await _deptoProvider.modificarDeptos(deptos);
 _cargandoController.sink.add(false);

}
void borrarDeptos(DeptoModel deptos)async{


  _cargandoController.sink.add(true);
 await _deptoProvider.modificarDeptos(deptos);
 _cargandoController.sink.add(false);


}




dispose(){
_deptoController?.close();
_cargandoController?.close();

}


} 
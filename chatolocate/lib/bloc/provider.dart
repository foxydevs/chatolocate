

import 'package:chatolocate/bloc/colonias_bloc.dart';
import 'package:chatolocate/bloc/depto_bloc.dart';
import 'package:chatolocate/bloc/munis_bloc.dart';
import 'package:chatolocate/bloc/sucursales_bloc.dart';
import 'package:chatolocate/bloc/zonas_bloc.dart';
import 'package:flutter/material.dart';


import 'login_bloc.dart';


 
class Provider extends InheritedWidget {
      final _sucursalesBloc =SucursalesBloc();
      final _coloniasBloc = ColoniasBloc();
      final _zonasBloc = ZonasBloc();
      final _munisBloc = MunisBloc();
      final _deptoBloc = DeptoBloc();
       final _loginBloc = LoginBloc();

      
     


  static Provider _instancia;

  factory Provider({ Key key, Widget child }) {

    if ( _instancia == null ) {
      _instancia = new Provider._internal(key: key, child: child );
    }
     
  
    return _instancia;

  }

  Provider._internal({ Key key, Widget child })
    : super(key: key, child: child );




  // Provider({ Key key, Widget child })
  //   : super(key: key, child: child );

 
  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;
 

 static LoginBloc loginBloc ( BuildContext context ) {
    return  context.dependOnInheritedWidgetOfExactType<Provider>()._loginBloc;
  }
 static DeptoBloc deptoBloc ( BuildContext context ) {
    return  context.dependOnInheritedWidgetOfExactType<Provider>()._deptoBloc;
  }
   static MunisBloc munisBloc ( BuildContext context ) {
    return  context.dependOnInheritedWidgetOfExactType<Provider>()._munisBloc;
  }
  static ZonasBloc zonasBloc ( BuildContext context ) {
    return  context.dependOnInheritedWidgetOfExactType<Provider>()._zonasBloc;
  }
    static ColoniasBloc coloniasBloc ( BuildContext context ) {
    return  context.dependOnInheritedWidgetOfExactType<Provider>()._coloniasBloc;
  }
    static SucursalesBloc sucursalesBloc ( BuildContext context ) {
    return  context.dependOnInheritedWidgetOfExactType<Provider>()._sucursalesBloc;
  }
  static of(BuildContext context) {}



}
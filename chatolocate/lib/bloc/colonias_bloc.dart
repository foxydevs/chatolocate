    
import 'dart:io';



import 'package:chatolocate/models/colonias_model.dart';
import 'package:chatolocate/models/munis_model.dart';
import 'package:chatolocate/models/zonas_model.dart';
import 'package:chatolocate/provider/colonias_provider.dart';
import 'package:chatolocate/provider/munis_provider.dart';
import 'package:chatolocate/provider/zonas_provider.dart';
import 'package:rxdart/subjects.dart';
 
class ColoniasBloc{

final _coloniasController = new BehaviorSubject<List<ColoniasModel>>();

final _cargandoController = new BehaviorSubject<bool>();

final _coloniasProvider = new ColoniasProvider();

Stream<List<ColoniasModel>> get coloniasStream => _coloniasController.stream;

Stream<bool> get cargando => _cargandoController.stream;
 
Future<Null> cargarColonias()async{

final colonias = await _coloniasProvider.cargarColonias();
_coloniasController.sink.add(colonias);

}
Future<Null> cargarColoniasid(String iddir)async{

final colonias = await _coloniasProvider.cargarColoniasid(iddir);
_coloniasController.sink.add(colonias);



}

 agregarColonias(ColoniasModel colonias)async{
 
 _cargandoController.sink.add(true);
 await _coloniasProvider.crearColonias(colonias);
 _cargandoController.sink.add(false);

}
//void cuando no regresa nada 
Future subirFoto(File foto)async{

 _cargandoController.sink.add(true);
 final fotoUrl = await _coloniasProvider.subirImagen(foto); 
 _cargandoController.sink.add(false);
  return fotoUrl;
}

void editarColonias(ColoniasModel colonias)async{
 
 _cargandoController.sink.add(true);
 await _coloniasProvider.modificarColonias(colonias);
 _cargandoController.sink.add(false);

}
void borrarColonias(ColoniasModel colonias)async{


  _cargandoController.sink.add(true);
 await _coloniasProvider.modificarColonias(colonias);
 _cargandoController.sink.add(false);


}




dispose(){
_coloniasController?.close();
_cargandoController?.close();

}


} 
   
import 'dart:io';



import 'package:chatolocate/models/munis_model.dart';
import 'package:chatolocate/provider/munis_provider.dart';
import 'package:rxdart/subjects.dart';
 
class MunisBloc{

final _munisController = new BehaviorSubject<List<MunisModel>>();

final _cargandoController = new BehaviorSubject<bool>();

final _munisProvider = new MunisProvider();

Stream<List<MunisModel>> get munisStream => _munisController.stream;

Stream<bool> get cargando => _cargandoController.stream;
 
Future<Null> cargarMunis()async{

final munis = await _munisProvider.cargarMunis();
_munisController.sink.add(munis);

}
Future<Null> cargarMunisid(String iddir)async{

final munis = await _munisProvider.cargarMunisid(iddir);
_munisController.sink.add(munis);



}

 agregarMunis(MunisModel munis)async{
 
 _cargandoController.sink.add(true);
 await _munisProvider.crearMunis(munis);
 _cargandoController.sink.add(false);

}
//void cuando no regresa nada 
Future subirFoto(File foto)async{

 _cargandoController.sink.add(true);
 final fotoUrl = await _munisProvider.subirImagen(foto); 
 _cargandoController.sink.add(false);
  return fotoUrl;
}

void editarMunis(MunisModel munis)async{
 
 _cargandoController.sink.add(true);
 await _munisProvider.modificarMunis(munis);
 _cargandoController.sink.add(false);

}
void borrarMunis(MunisModel munis)async{


  _cargandoController.sink.add(true);
 await _munisProvider.modificarMunis(munis);
 _cargandoController.sink.add(false);


}




dispose(){
_munisController?.close();
_cargandoController?.close();

}


} 
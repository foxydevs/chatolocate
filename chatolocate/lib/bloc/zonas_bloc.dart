   
import 'dart:io';



import 'package:chatolocate/models/munis_model.dart';
import 'package:chatolocate/models/zonas_model.dart';
import 'package:chatolocate/provider/munis_provider.dart';
import 'package:chatolocate/provider/zonas_provider.dart';
import 'package:rxdart/subjects.dart';
 
class ZonasBloc{

final _zonasController = new BehaviorSubject<List<ZonasModel>>();

final _cargandoController = new BehaviorSubject<bool>();

final _zonasProvider = new ZonasProvider();

Stream<List<ZonasModel>> get zonasStream => _zonasController.stream;

Stream<bool> get cargando => _cargandoController.stream;
 
Future<Null> cargarZonas()async{

final zonas = await _zonasProvider.cargarZonas();
_zonasController.sink.add(zonas);

}
Future<Null> cargarZonasid(String iddir)async{

final zonas = await _zonasProvider.cargarZonasid(iddir);
_zonasController.sink.add(zonas);



}

 agregarZonas(ZonasModel zonas)async{
 
 _cargandoController.sink.add(true);
 await _zonasProvider.crearZonas(zonas);
 _cargandoController.sink.add(false);

}
//void cuando no regresa nada 
Future subirFoto(File foto)async{

 _cargandoController.sink.add(true);
 final fotoUrl = await _zonasProvider.subirImagen(foto); 
 _cargandoController.sink.add(false);
  return fotoUrl;
}

void editarZonas(ZonasModel zonas)async{
 
 _cargandoController.sink.add(true);
 await _zonasProvider.modificarZonas(zonas);
 _cargandoController.sink.add(false);

}
void borrarZonas(ZonasModel zonas)async{


  _cargandoController.sink.add(true);
 await _zonasProvider.modificarZonas(zonas);
 _cargandoController.sink.add(false);


}




dispose(){
_zonasController?.close();
_cargandoController?.close();

}


} 